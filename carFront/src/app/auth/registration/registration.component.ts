import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {FormControl, FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  fieldRegister: FormGroup


  constructor(private fb: FormBuilder ,private auth:AuthService) {

    this.fieldRegister = this.fb.group({

      email: this.fb.control('email',[
        Validators.required,
        Validators.minLength(4),
        Validators.email
      ]),
      password: this.fb.control('password',[
        Validators.required,
        Validators.minLength(4),
        //Validators.pattern('(?=.*\d)(?=.*[\W_])(?=.*[A-Z])(?=.*[a-z]).+')
      ]),
      passwordRepeat: this.fb.control('password',[
        Validators.required,
        Validators.minLength(4)
      ])
    })
   }

   register(){
    this.auth.register(this.fieldRegister.value)
  }

  ngOnInit(): void {

  }

}

import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {map, tap} from 'rxjs/operators';

interface Edit {
  email: string;
  name: string;
  bestCar: string;
}

interface Credentials {
  email: string;
  password: string;
}

interface Session {
  success: boolean;
  accessToken: string;
  refreshToken: string;
}

interface Refresh {
  refreshToken: string;
}


@Injectable()
export class AuthService {

  private session = new BehaviorSubject<Session>(null);
  public error = new BehaviorSubject<any>(null);
  email = new BehaviorSubject<any>(null);


  /*
    private refresh = new BehaviorSubject<Refresh>(null)
  */

  getaceToken() {
    const session = this.session.getValue();
    return session && session.accessToken;
  }

  getrefToken() {
    const session = this.session.getValue();
    return session && session.refreshToken;
  }

  getSuccess() {
    const session = this.session.getValue();
    return session && session.success;
  }


  isAuthenticated = false;

  state = this.session.pipe(
    map(session => !!session),
    tap(state => this.isAuthenticated = state)
  );

  editProfile(editData: Edit) {
    console.log(editData);
  }


  logout(refToken: Refresh) {
    this.http.post('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/auth-manager/logout', refToken)
      .subscribe(response => {
        console.log(response);
      });
    this.session.next(null);

    this.router.navigate(['/']);
  }

  login(credentials: Credentials) {
    this.http.post('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/auth-manager/login', credentials)
      .subscribe((session: Session) => {
          this.session.next(session);
          this.router.navigate(['/dashboard']);

        },
        error => {
          if (error instanceof HttpErrorResponse) {
            console.error(error.error);
            this.error.next(error);
          }
        });
    this.email.next(credentials.email);

  }

loginFB(){
  window['FB'].getLoginStatus((response) => {

    console.log('login response',response);
   if (response.authResponse) {

      window['FB'].api('/me', {
        fields: 'last_name, first_name, email'
      }, (userInfo) => {

        console.log("user information");
        console.log(userInfo);
        this.session.next(userInfo);
      });

    } else {
      console.log('User login failed');
    }
}, {scope: 'public_profile,mail'});
}



  register(credentials: Credentials) {
    {
      this.http.post('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/auth-manager/register', credentials)
        .subscribe((session: Session) => {
          this.session.next(session);
          this.router.navigate(['/dashboard']);
        });
      this.email.next(credentials.email);
    }
  }

  constructor(private http: HttpClient, public router: Router) {
  }
}



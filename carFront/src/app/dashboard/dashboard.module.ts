import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardService } from './dashboard.service';
import { EditCarComponent } from './edit-car/edit-car.component';
import { EditExpensesComponent } from './edit-expenses/edit-expenses.component';




@NgModule({
  declarations: [DashboardComponent, EditCarComponent, EditExpensesComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [DashboardComponent, EditCarComponent, EditExpensesComponent],
  providers: [DashboardService]
})
export class DashboardModule { }

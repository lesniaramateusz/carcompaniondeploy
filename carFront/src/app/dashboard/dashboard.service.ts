import { HttpClient, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay,map, tap } from 'rxjs/operators';

interface postCar{
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage: number;
  productionYear: number;
}


interface Expense{
  amount: number;
  description: string;
  mileageInterval: number;
  date: string;
  endOfDateInterval: string;
  category: string;
}

interface Share{
  success: boolean;
  shareKey: string;
}
interface Date{
  startDate: string;
  endDate: string;
}



@Injectable()
export class DashboardService {
  allexpenses = new BehaviorSubject<any>(null);
  allcategory = new BehaviorSubject<any>(null);
  allcars = new BehaviorSubject<any>(null);
  carid = new BehaviorSubject<any>(null);
  expenseId = new BehaviorSubject<any>(null);
  shareKey = new BehaviorSubject<any>(null);
  summary = new BehaviorSubject<any>(null);
  adminLogs = new BehaviorSubject<any>(null);
  userLogs = new BehaviorSubject<any>(null);
  url='http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/v1/cars/';
  urllogs='http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/v1/';

  shareCar(fieldCar, id){
    this.http.post(this.url+id+'/share',fieldCar).subscribe((response:Share) =>{
      this.shareKey.next(response.shareKey)
    })
  }

  getCarById(id){
    this.carid.next(id);
  }

  getExpenseId(id){
    this.expenseId.next(id);
  }
  updateExpense(expense, idcar, expid){
    this.http.patch(this.url+idcar+'/expenses/'+expid, expense).subscribe(response=>{
      console.log(response);
    })
  }
  deleteExpense(carid, expid){
    this.http.delete(this.url+carid+'/expenses/'+expid).subscribe(response=>{
      console.log(response);
    })
  }
  getSummary(carId, date:Date){
    this.http.get(this.url+carId+'/summary?StartDate='+date.startDate+'&EndDate='+date.endDate).subscribe(response => {
      this.summary.next(response);
    })
  }

  postCar(car:postCar){
     this.http.post<postCar>('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/v1/cars',car).subscribe(response =>{
      console.log(response);
      })
  }

  getCars(){
    this.http.get<any>('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/v1/cars').subscribe(response =>{
      this.allcars.next(response.cars);
    })
  }

  updateCar(car:postCar,id){
    this.http.patch<postCar>(this.url+id, car).subscribe(response=>{
      console.log(response);
    })
    this.router.navigate(['/dashboard']);
  }

  addExpense(expense:Expense, id){
    this.http.post<Expense>(this.url+id+'/expenses',expense).subscribe(response=>{
      this.summary.next(response);
    })
  }

  getExpense(id){
    this.http.get<any>(this.url+id+'/expenses').subscribe(response =>{
      this.allexpenses.next(response);
    })
  }

  getCategory(){
    this.http.get('http://carcomapanionbackend.germanywestcentral.azurecontainer.io/api/v1/cars/expenses/categories').subscribe(response =>{
      this.allcategory.next(response);
    })
  }

  deleteCar(id){
    this.http.delete(this.url+id).subscribe(response =>{
      console.log(response);
    })
    setTimeout(()=>{this.router.navigate(['/dashboard']),3000});
  }

  subscribeCare(key){
    this.http.post(this.url+'use-sharekey/'+key, null).subscribe(response =>{
      console.log(response);
    });
  }
  getLogs(){
    this.http.get(this.urllogs+'logs?PerPage='+'100&Page='+'100&StartDate='+'1900-01-01&EndDate='+'2137-01-01&SortOrder='+'oldest').subscribe(response =>{
      this.adminLogs.next(response);
    })
    this.http.get(this.urllogs+'logs/'+'my-logs?PerPage='+'100&Page='+'100&StartDate='+'1900-01-01&EndDate='+'2137-01-01&SortOrder='+'oldest').subscribe(response =>{
      this.userLogs.next(response);
    })
  }

  constructor(private http:HttpClient, private router: Router) { }


}

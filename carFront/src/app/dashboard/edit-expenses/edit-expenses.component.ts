import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';

interface Expenses {
    expenseId: string;
    amount: number;
    carId: string;
    ​​​​description: string;
    ​​​​date: string;
    ​​​​endOfDateInterval: string;
    ​​​​category: string;
    ​​mileageInterval: number;
    ​​userId: string;
}

@Component({
  selector: 'app-edit-expenses',
  templateUrl: './edit-expenses.component.html',
  styleUrls: ['./edit-expenses.component.css']
})
export class EditExpensesComponent implements OnInit {

  constructor(private dash: DashboardService, private fb: FormBuilder, public router : Router) { }

  exp;
  Category: any = ['fuel', 'insurance', 'other', 'repair', 'utilization'];
  editExpense = this.fb.group({
    amount: this.fb.control('amount',[
      Validators.max(10000000000000000),
      Validators.min(0),
    ]),
    description: this.fb.control('description',[
      Validators.maxLength(256)
    ]),
    mileageInterval: this.fb.control('mileageInterval',[
      Validators.max(1000000),
      Validators.min(0)
    ]),
    endOfDateInterval: this.fb.control('endOfDateInterval',[

    ]),
    category: this.fb.control('category',[
      Validators.required,
    ])
  });

  updateExpense(){
    this.dash.updateExpense(this.editExpense.value, this.exp.carId, this.exp.expenseId);
    setTimeout(()=>{this.router.navigate(['/carEdit'])},1000);

  }

  deleteExpense(){
    this.dash.deleteExpense(this.exp.carId, this.exp.expenseId);
    setTimeout(()=>{this.router.navigate(['/carEdit'])},1000);
  }


  ngOnInit(): void {
    this.dash.expenseId.subscribe((response:Expenses) =>{
      this.exp = response;
      console.log(this.exp);
    })
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';


interface Cars {
  carId: string;
  mainName: string;
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage: number;
  productionYear: number;
}

interface Expenses {
  carId:string;
  expenses: {
    expenseId: string;
    amount: number;
    carId: string;
    description: string;
    date: string;
    endOfDateInterval: string;
    category: string;
    mileageInterval: number;
    userId: string;
  }
}
interface CategoriesSummary{
  expensesCount: number;
  expensesTotalAmount: number;
  categoriesSummary: {
     categoryExpensesCount: number;
    categoryName: string;
     categoryTotalAmount: number;
  }
}

@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {
  @ViewChild('barCanvas') barCanvas;
  editCar = this.fb.group({
    mainName: this.fb.control('mainName',[
      Validators.maxLength(30),
    ]),
    brand: this.fb.control('brand',[
      Validators.maxLength(30),
    ]),
    model: this.fb.control('model',[
      Validators.maxLength(30),
    ]),
    generation: this.fb.control('generation',[
      Validators.maxLength(30),
    ]),
    plate: this.fb.control('plate',[
      Validators.maxLength(10)
    ]),
    mileage: this.fb.control('mileage',[
      Validators.max(5000000),
      Validators.min(0)
    ]),
    productionYear: this.fb.control('productionYear',[
      Validators.min(1900),
      Validators.max(3000)
    ])
  });

  summary = this.fb.group({
    startDate: this.fb.control('startDate', [
      Validators.required
    ]),
    endDate: this.fb.control('endDate', [
      Validators.required
    ])
  })



  Role: any = ['editor', 'viewer'];
  Category: any = ['fuel', 'insurance', 'other', 'repair', 'utilization'];

  shareField = this.fb.group({
    role:['']
  });

  postExpense = this.fb.group({
    amount: this.fb.control('amount',[
      Validators.max(10000000000000000),
      Validators.min(0),
    ]),
    description: this.fb.control('description',[
      Validators.maxLength(256)
    ]),
    mileageInterval: this.fb.control('mileageInterval',[
      Validators.max(1000000),
      Validators.min(0)
    ]),
    endOfDateInterval: this.fb.control('endOfDateInterval',[

    ]),
    category: this.fb.control('category',[
      Validators.required,
    ]),
    date : this.fb.control('date',[

    ])
  });

 sumdata;
 key;
 exp;
 car;
 deleted = false;
  constructor(private fb:FormBuilder, private dash:DashboardService, public router: Router) {
   }

   shareCar(){
     this.dash.shareCar(this.shareField.value,this.car.carId);
   }

  updateCar(){
    this.dash.updateCar(this.editCar.value,this.car.carId);
  }
  deleteCar(){
    this.dash.deleteCar(this.car.carId);
      this.deleted = true;
  }
  editExpense(e){
    this.dash.getExpenseId(e);
    setTimeout(()=>{this.router.navigate(['/expenseEdit'])},1000);
  }

  addExpense(){
    this.dash.addExpense(this.postExpense.value,this.car.carId);
    setTimeout(()=>{this.ngOnInit()},1000);
  }
  getSummary(){
    console.log(this.summary.value)
    this.dash.getSummary(this.car.carId, this.summary.value);
    setTimeout(()=>{this.ngOnInit()},1000);
  }





  ngOnInit(): void {

 //   this.dash.getCategory();
 //   this.dash.allcategory.subscribe(response =>{
  //    this.category = response;
  //    console.log(response);
  //  });
  //  this.dash.allexpenses.subscribe(response=>{
    //  this.exp = response;
  //    console.log('editcar ma wydatki '+response)
  //  });
    this.dash.summary.subscribe((response: CategoriesSummary) => {
      this.sumdata = response;
      console.log('edit car ma summary:'+ this.sumdata.expensesTotalAmount);
    })
    this.dash.shareKey.subscribe(response => {
      this.key = response;
    });
    this.dash.carid.subscribe((response:Cars) => {
      this.car = response;
     console.log('edit component to ma',this.car)
    });
    this.dash.getExpense(this.car.carId);
    this.dash.allexpenses.subscribe((response: Expenses) => {
        this.exp = response;
       console.log('editcar ma wydatki '+this.exp);
    });

  }
}

import { Component, OnInit, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from './dashboard.service';
import {Router} from '@angular/router';

interface getCars{
  userId:string;
  carId: string;
  mainName: string;
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage:number;
  productionYear: number;
  users:[]
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {


  cars: Array<getCars> = [];

  shareKey = this.fb.group({
    key:['']
  })

  postCar = this.fb.group({
    mainName: this.fb.control('mainName',[
      Validators.maxLength(30),
    ]),
    brand: this.fb.control('brand',[
      Validators.maxLength(30),
    ]),
    model: this.fb.control('model',[
      Validators.maxLength(30),
    ]),
    generation: this.fb.control('generation',[
      Validators.maxLength(30),
    ]),
    plate: this.fb.control('plate',[
      Validators.maxLength(10)
    ]),
    mileage: this.fb.control('mileage',[
      Validators.max(5000000),
      Validators.min(0),
    ]),
    productionYear: this.fb.control('productionYear',[
      Validators.min(1900),
      Validators.max(3000),
    ])
  });

  logiuser;
  logiadmin;

  constructor(private fb: FormBuilder, private dash: DashboardService, public router: Router) {}


  consolelog(cars) {
    console.log(cars);
  }

  addCar() {
    this.dash.postCar(this.postCar.value);
    setTimeout(()=>{this.ngOnInit()},1000);

  }

  getCarById(editcar:any){
    this.dash.getCarById(editcar);
    setTimeout(()=>{this.router.navigate(['/carEdit'])},500);
  }

  shareCar(){
    console.log('dashboard Component to ma '+this.shareKey.value.key)
    this.dash.subscribeCare(this.shareKey.value.key);
    setTimeout(()=>{this.ngOnInit()},1000);
  }
  getLogs(){
    this.dash.getLogs();
    setTimeout(()=>{this.ngOnInit()},1000);
  }

  ngOnInit(): void {

   this.dash.getCars();
    this.dash.allcars.subscribe(response => {
    this.cars = response;
      this.consolelog(this.cars);
    });
    this.dash.adminLogs.subscribe(response =>{
      this.logiadmin = response;
    })
    this.dash.userLogs.subscribe(response =>{
      this.logiuser = response;
    })
  }
}

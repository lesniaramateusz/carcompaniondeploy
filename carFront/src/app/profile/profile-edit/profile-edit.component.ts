import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

   editForm = this.fb.group({
  username:[''],
  email:[''],
  bestCar:['']
})


  constructor(private fb:FormBuilder, private auth:AuthService) {
  }

  editProfile(){
    this.auth.editProfile(this.editForm.value)
  }

  ngOnInit(): void {
  }

}

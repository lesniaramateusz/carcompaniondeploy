import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-profile-bar',
  templateUrl: './profile-bar.component.html',
  styleUrls: ['./profile-bar.component.css']
})
export class ProfileBarComponent implements OnInit {

   user


  constructor(public auth: AuthService) {
    this.auth.state.subscribe()
  }



  ngOnInit(){
  this.auth.email.subscribe( user =>{
    this.user = user
  })
  }
}
